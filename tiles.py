import libtcodpy as libtcod



class Tile():

  def __init__(self, letter, color, passable):
    self.display = (letter, color)
    self.passable = passable
    self.items = []

  def get_display(self):
    return self.display

  def add_item(self, item):
    self.items.append(item)

  def remove_item(self, item):
    self.items.remove(item)

  def is_passable(self):
    return self.passable


class FloorTile(Tile):

  def __init__(self):
    Tile.__init__(self, 250, libtcod.darker_gray, True)


class BoxTile(Tile):

  def __init__(self):
    Tile.__init__(self, libtcod.CHAR_BULLET_SQUARE, libtcod.sepia, True)


class BodyTile(Tile):

  def __init__(self):
    Tile.__init__(self, '@', libtcod.gray, True)


class WallTile(Tile):

  def __init__(self):
    Tile.__init__(self, "#", libtcod.lighter_gray, False)


class StairTile(Tile):

  def __init__(self, rotation):
    letter = [libtcod.CHAR_HLINE, "/",
              libtcod.CHAR_VLINE, "\\"][rotation % 4]
    Tile.__init__(self, letter, libtcod.darker_gray, True)


class UpTile(Tile):

  def __init__(self, rot):
    self.rot = rot
    Tile.__init__(self, ">", libtcod.yellow, True)


class DownTile(Tile):

  def __init__(self, rot):
    self.rot = rot
    Tile.__init__(self, "<", libtcod.yellow, True)


class DoorTile(Tile):

  def __init__(self, vertical, connecting, random):
    self.vertical = vertical
    self.connecting = connecting

    self.is_open = bool(libtcod.random_get_int(random, 0, 1))
    self.is_locked = False
    if not self.connecting:
      self.is_locked = bool(libtcod.random_get_int(random, 0, 1))

    Tile.__init__(self, "/", libtcod.sepia, True)

  def get_display(self):
    if self.is_open:
      return Tile.get_display(self)

    if self.vertical:
      return libtcod.CHAR_VLINE, libtcod.sepia
    else:
      return libtcod.CHAR_HLINE, libtcod.sepia

  def is_passable(self):
    return self.is_open


class ExitDoorTile(Tile):

  def __init__(self, vertical):
    if vertical:
      Tile.__init__(self, libtcod.CHAR_VLINE, libtcod.gray, False)
    else:
      Tile.__init__(self, libtcod.CHAR_HLINE, libtcod.gray, False)


