import libtcodpy as libtcod


MAX_VALUE = 400
MAX_VALUE_SCALING = 50
MIN_VALUE_SCALING = 80

ITEM_MATERIALS = [
  ("tin", 0),
  ("copper", 10),
  ("bronze", 25),
  ("silver", 60),
  ("golden", 150),
]

ITEM_MODIFIERS = [
  ("boring", 0),
  ("mundane", 2),
  ("plain", 4),
  ("interesting", 8),
  ("fine", 10),
  ("fancy", 16),
  ("ornamental", 24),
  ("engraved", 36),
  ("filigreed", 64),
  ("gem encrusted", 100),
  ("gaudy", 150),
]

ITEM_TYPES = [
  ("cup", 1),
  ("plate", 1),
  ("ring", 2),
  ("bracelet", 2),
  ("trinket", 3),
  ("amulet", 3),
  ("box", 4),
  ("idol", 4),
]



class Item():

  def __init__(self, name):
    self.name = name

  def get_name(self):
    return self.name


class Torch(Item):

  def __init__(self):
    Item.__init__(self, "torch")


class BurntTorch(Item):

  def __init__(self):
    Item.__init__(self, "partially burnt torch")


class Valuable(Item):

  def __init__(self, level, random):

    max_value = (MAX_VALUE * level) / (MAX_VALUE_SCALING + level)
    min_value = (MAX_VALUE * level) / (MIN_VALUE_SCALING + level)

    base_value = libtcod.random_get_int(random, max(min_value, 1),
                                        max(max_value, 1))

    for name, value in ITEM_MATERIALS:
      if value > base_value:
        break
      material_name, material_value = name, value

    for name, value in ITEM_MODIFIERS:
      if value > base_value - material_value:
        break
      modifier_name, modifier_value = name, value

    type_name, type_multiplier = ITEM_TYPES[libtcod.random_get_int(random,
                                            0, len(ITEM_TYPES) - 1)]

    name = modifier_name + " " + material_name + " " + type_name
    self.value = base_value * type_multiplier + \
                 libtcod.random_get_int(random, 0, type_multiplier - 1)

    Item.__init__(self, name)


