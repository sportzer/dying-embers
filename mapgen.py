import libtcodpy as libtcod
import tiles
import items


REMOVE_DOOR_ITERATIONS = 2**8
MAX_STAIRS = 3
NEW_STAIR_CHANCE = 0.5

MAX_BODIES = 5
MAX_BODIES_SCALING = 30
MIN_BODIES_SCALING = 10



class Stairs():

  def __init__(self, x, y, rot, bottom):
    self.x = x
    self.y = y
    self.rot = rot
    self.bottom = bottom

  def propagate(self):
    return Stairs(self.x, self.y, self.rot, False)



class Map():

  def __init__(self, random):

    self.random = random

    x = libtcod.random_get_int(self.random, 0, 13)
    y = libtcod.random_get_int(self.random, 0, 13)
    rot = libtcod.random_get_int(self.random, 0, 3)
    self.top_stairs = [Stairs(x, y, rot, True)]

    self.floors = []
    self.gen_next_floor()


  def gen_next_floor(self):

    #### Partition the floor into grid rectangles ####
    xlines = set([0,16])
    ylines = set([0,16])

    for stairs in self.top_stairs:
      xlines.add(stairs.x)
      xlines.add(stairs.x + 3)
      ylines.add(stairs.y)
      ylines.add(stairs.y + 3)

    xlines = self.subdivide_intervals(xlines)
    ylines = self.subdivide_intervals(ylines)

    unclaimed = set()
    for x in xrange(0, len(xlines) - 1):
      for y in xrange(0, len(ylines) - 1):
        unclaimed.add((x,y))

    #### Maybe put in a new staircase ####
    if len(self.top_stairs) < MAX_STAIRS and \
        libtcod.random_get_float(self.random, 0, 1) < NEW_STAIR_CHANCE:
      x3wide = []
      for i in xrange(0, len(xlines) - 1):
        if xlines[i+1] - xlines[i] == 3:
          x3wide.append(i)

      y3wide = []
      for i in xrange(0, len(ylines) - 1):
        if ylines[i+1] - ylines[i] == 3:
          y3wide.append(i)

      x = x3wide[libtcod.random_get_int(self.random, 0, len(x3wide) - 1)]
      y = y3wide[libtcod.random_get_int(self.random, 0, len(y3wide) - 1)]

      for stairs in self.top_stairs:
        if xlines[x] == stairs.x and ylines[y] == stairs.y:
          break
      else:
        rot = libtcod.random_get_int(self.random, 0, 3)
        self.top_stairs.append(Stairs(xlines[x], ylines[y], rot, True))

    stair_dict = {}

    for stairs in self.top_stairs:
      for i in xrange(0, len(xlines) - 1):
        if xlines[i] == stairs.x:
          break
      else:
        raise Exception("Should not happen")

      for j in xrange(0, len(ylines) - 1):
        if ylines[j] == stairs.y:
          break
      else:
        raise Exception("Should not happen")

      stair_dict[(i,j)] = stairs
      unclaimed.discard((i,j))

    #### Divide the grid up into rooms ####
    claims = {}
    hallways = set()
    next_room_id = 0
    while unclaimed:
      x, y = iter(unclaimed).next()
      extensions = [(x,y,x,y)]

      if xlines[x+1] - xlines[x] == 1 or ylines[y+1] - ylines[y] == 1:
        hall = True

        for dx, dy in [(1,0), (-1,0), (0,1), (0,-1)]:
          if (x+dx, y+dy) in unclaimed:
            if xlines[x+1+dx] - xlines[x+dx] == 1 or \
                ylines[y+1+dy] - ylines[y+dy] == 1:
              extensions.append((min(x,x+dx), min(y,y+dy),
                                 max(x,x+dx), max(y,y+dy)))

      else:
        hall = False

        if (x-1, y) in unclaimed:
          if xlines[x] - xlines[x-1] == 1:
            if (x-2, y) in unclaimed:
              extensions.append((x-2,y,x,y))
            elif x == 1:
              extensions.append((x-1,y,x,y))
          else:
            extensions.append((x-1,y,x,y))

        if (x+1, y) in unclaimed:
          if xlines[x+2] - xlines[x+1] == 1:
            if (x+2, y) in unclaimed:
              extensions.append((x,y,x+2,y))
            elif x == len(xlines) - 2:
              extensions.append((x,y,x+1,y))
          else:
            extensions.append((x,y,x+1,y))

        for x1, y1, x2, y2 in extensions[:]:
          if y1 > 0:
            if y1 > 1 and ylines[y1] - ylines[y1-1] == 1:
              for x in xrange(x1, x2 + 1):
                if (x, y1-1) not in unclaimed or (x, y1-2) not in unclaimed:
                  break
              else:
                extensions.append((x1,y1-2,x2,y2))
            else:
              for x in xrange(x1, x2 + 1):
                if (x, y1-1) not in unclaimed:
                  break
              else:
                extensions.append((x1,y1-1,x2,y2))

          if y1 < len(ylines) - 2:
            if y1 < len(ylines) - 3 and ylines[y1+2] - ylines[y1+1] == 1:
              for x in xrange(x1, x2 + 1):
                if (x, y1+1) not in unclaimed or (x, y1+2) not in unclaimed:
                  break
              else:
                extensions.append((x1,y1,x2,y2+2))
            else:
              for x in xrange(x1, x2 + 1):
                if (x, y1-1) not in unclaimed:
                  break
              else:
                extensions.append((x1,y1,x2,y2+1))

      x1, y1, x2, y2 = extensions[libtcod.random_get_int(self.random, 0,
                                                         len(extensions)-1)]

      for x in xrange(x1, x2 + 1):
        for y in xrange(y1, y2 + 1):
          # This is kind of terrible since (x, y) shouldn't be claimed
          # but sometimes it is, and basically we just get more
          # interesting rooms. So... win win? As a side note you sometimes
          # get double wide hallways for the non-obvious reason that
          # two normal hallways next to each other will get merged.
          unclaimed.discard((x, y))
          claims[(x,y)] = next_room_id

      if hall:
        hallways.add(next_room_id)
      next_room_id += 1

    for pt in stair_dict:
      claims[pt] = - self.top_stairs.index(stair_dict[pt]) - 1

    #### Enumerate walls, door, connected hallways ####
    path_dict = {}
    for i in xrange(-len(self.top_stairs), next_room_id):
      for j in xrange(-len(self.top_stairs), i):
        path_dict[(i,j)] = path_dict[(j,i)] = set()

    exits = {}
    for i in xrange(-len(self.top_stairs), next_room_id):
      exits[i] = []

    walls = set()
    connected_halls = dict([(h, []) for h in hallways])
    for x in xrange(0, len(xlines) - 1):
      for y in xrange(0, len(ylines) - 1):
        if x == 0 or claims[(x,y)] != claims[(x-1,y)]:
          if x != 0 and claims[(x,y)] in hallways and claims[(x-1,y)] in hallways:
            connected_halls[claims[(x,y)]].append(claims[(x-1,y)])
            connected_halls[claims[(x-1,y)]].append(claims[(x,y)])
          else:
            for ypos in xrange(2 * ylines[y], 2 * ylines[y+1] + 1):
              walls.add((2 * xlines[x], ypos))
            door = (2 * xlines[x], ylines[y] + ylines[y+1])
            if x != 0:
              path_dict[(claims[(x,y)], claims[(x-1,y)])].add(door)
            else:
              exits[claims[(x,y)]].append(door)

        if x == len(xlines) - 2 or claims[(x,y)] != claims[(x+1,y)]:
          if x != len(xlines) - 2 and claims[(x,y)] in hallways and claims[(x+1,y)] in hallways:
            connected_halls[claims[(x,y)]].append(claims[(x+1,y)])
            connected_halls[claims[(x+1,y)]].append(claims[(x,y)])
          else:
            for ypos in xrange(2 * ylines[y], 2 * ylines[y+1] + 1):
              walls.add((2 * xlines[x+1], ypos))
            door = (2 * xlines[x+1], ylines[y] + ylines[y+1])
            if x != len(xlines) - 2:
              path_dict[(claims[(x,y)], claims[(x+1,y)])].add(door)
            else:
              exits[claims[(x,y)]].append(door)

        if y == 0 or claims[(x,y)] != claims[(x,y-1)]:
          if y != 0 and claims[(x,y)] in hallways and claims[(x,y-1)] in hallways:
            connected_halls[claims[(x,y)]].append(claims[(x,y-1)])
            connected_halls[claims[(x,y-1)]].append(claims[(x,y)])
          else:
            for xpos in xrange(2 * xlines[x], 2 * xlines[x+1] + 1):
              walls.add((xpos, 2 * ylines[y]))
            door = (xlines[x] + xlines[x+1], 2 * ylines[y])
            if y != 0:
              path_dict[(claims[(x,y)], claims[(x,y-1)])].add(door)
            else:
              exits[claims[(x,y)]].append(door)

        if y == len(ylines) - 2 or claims[(x,y)] != claims[(x,y+1)]:
          if y != len(ylines) - 2 and claims[(x,y)] in hallways and claims[(x,y+1)] in hallways:
            connected_halls[claims[(x,y)]].append(claims[(x,y+1)])
            connected_halls[claims[(x,y+1)]].append(claims[(x,y)])
          else:
            for xpos in xrange(2 * xlines[x], 2 * xlines[x+1] + 1):
              walls.add((xpos, 2 * ylines[y+1]))
            door = (xlines[x] + xlines[x+1], 2 * ylines[y+1])
            if y != len(ylines) - 2:
              path_dict[(claims[(x,y)], claims[(x,y+1)])].add(door)
            else:
              exits[claims[(x,y)]].append(door)

    for x, y in stair_dict:
      stairs = stair_dict[(x,y)]
      if stairs.rot == 0:
        dx, dy = 1, 0
      elif stairs.rot == 1:
        dx, dy = 0, -1
      elif stairs.rot == 2:
        dx, dy = -1, 0
      elif stairs.rot == 3:
        dx, dy = 0, 1
      door = (2 * xlines[x] + 3 + 3 * dx, 2 * ylines[y] + 3 + 3 * dy)
      if (x+dx, y+dy) in claims:
        path_dict[(claims[(x,y)], claims[(x+dx, y+dy)])].discard(door)
      else:
        exits[claims[(x,y)]].remove(door)

    #### Use random walk to remove doors without destroying connectivity ####
    for _ in xrange(REMOVE_DOOR_ITERATIONS):
      start_room = libtcod.random_get_int(self.random,
                                          -len(self.top_stairs),
                                          next_room_id - 1)
      if start_room in hallways:
        continue
      room_doors = []
      for next_room in xrange(-len(self.top_stairs), next_room_id):
        if next_room == start_room:
          continue
        for door in path_dict[(start_room, next_room)]:
          room_doors.append((next_room, door))
      if not room_doors: # really shouldn't happen, but eh.. oh well
        continue
      current_room, first_door = room_doors[libtcod.random_get_int(
                                   self.random, 0, len(room_doors)-1)]
      previous_room = current_door = None
      while current_room != start_room:
        room_doors = []
        for next_room in xrange(-len(self.top_stairs), next_room_id):
          if next_room == current_room:
            continue
          for door in path_dict[(current_room, next_room)]:
            room_doors.append((next_room, door))
        if current_room in hallways:
          for hall in connected_halls[current_room]:
            room_doors.append((hall, None))
        previous_room = current_room
        current_room, current_door = room_doors[libtcod.random_get_int(
                                       self.random, 0, len(room_doors)-1)]
      if current_door != first_door:
        path_dict[(start_room, previous_room)].remove(current_door)

    doors = set()
    for l in path_dict.values():
      doors.update(l)

    pathable_doors = set()
    exit_door = None

    #### Do stuff for bottom most floor if necessary ####
    if not self.floors:
      current_room = -1
      while not exits[current_room]:
        room_doors = []
        for next_room in xrange(-len(self.top_stairs), next_room_id):
          if next_room == current_room:
            continue
          for door in path_dict[(current_room, next_room)]:
            room_doors.append((next_room, door))
        if current_room in hallways:
          for hall in connected_halls[current_room]:
            room_doors.append((hall, None))
        current_room, current_door = room_doors[libtcod.random_get_int(
                                       self.random, 0, len(room_doors)-1)]
        pathable_doors.add(current_door)

      exit_door = exits[current_room][libtcod.random_get_int(
                             self.random, 0, len(exits[current_room])-1)]

    #### Process stairs for connections ####
    current_room = -1
    while True:
      room_doors = []
      for next_room in xrange(-len(self.top_stairs), next_room_id):
        if next_room == current_room:
          continue
        for door in path_dict[(current_room, next_room)]:
          room_doors.append((next_room, door))
      if current_room in hallways:
        for hall in connected_halls[current_room]:
          room_doors.append((hall, None))
      current_room, current_door = room_doors[libtcod.random_get_int(
                                     self.random, 0, len(room_doors)-1)]
      pathable_doors.add(current_door)
      if current_room < 0:
        break

    end_stairs = None
    if len(self.top_stairs) > 1:
      end_stairs = libtcod.random_get_int(self.random, 0, len(self.top_stairs)-1)
      if self.top_stairs[end_stairs].bottom or end_stairs == -current_room-1:
        end_stairs = None

    next_stairs = []
    next_stairs.append(self.top_stairs[-current_room-1].propagate())
    for i in xrange(0, len(self.top_stairs)):
      if i != - current_room - 1 and i != end_stairs:
        next_stairs.append(self.top_stairs[i].propagate())

    #### Create actual map ####
    floor = [[tiles.FloorTile() for j in xrange(0,33)] for i in xrange(0,33)]

    for x, y in walls:
      floor[x][y] = tiles.WallTile()

    for i in xrange(0, len(self.top_stairs)):
      stairs = self.top_stairs[i]
      stair_tiles = []
      for dx in xrange(-1,2):
        for dy in xrange(-1,2):
          stair_tiles.append((dx, dy, tiles.WallTile()))
      stair_tiles.append((2, 0, tiles.WallTile()))

      if stairs.bottom:
        stair_tiles.append((2, -1, tiles.WallTile()))
      else:
        stair_tiles.append((2, -1, tiles.DownTile(stairs.rot)))
        stair_tiles.append((2, -2, tiles.StairTile(stairs.rot * 2 + 1)))
        stair_tiles.append((1, -2, tiles.StairTile(stairs.rot * 2 + 2)))

      if i == end_stairs:
        stair_tiles.append((2, 1, tiles.WallTile()))
      else:
        stair_tiles.append((2, 1, tiles.UpTile(stairs.rot)))
        stair_tiles.append((2, 2, tiles.StairTile(stairs.rot * 2 + 7)))
        stair_tiles.append((1, 2, tiles.StairTile(stairs.rot * 2 + 6)))

      for dx, dy, tile in stair_tiles:
        if stairs.rot == 0:
          x = 2 * stairs.x + 3 + dx
          y = 2 * stairs.y + 3 + dy
        elif stairs.rot == 1:
          x = 2 * stairs.x + 3 + dy
          y = 2 * stairs.y + 3 - dx
        elif stairs.rot == 2:
          x = 2 * stairs.x + 3 - dx
          y = 2 * stairs.y + 3 - dy
        else:
          x = 2 * stairs.x + 3 - dy
          y = 2 * stairs.y + 3 + dx

        floor[x][y] = tile

    for x, y in doors:
      floor[x][y] = tiles.DoorTile(x % 2 == 0 and x / 2 in xlines,
                                   (x, y) in pathable_doors, self.random)

    if exit_door:
      x, y = exit_door
      floor[x][y] = tiles.ExitDoorTile(x == 0 or x == 32)
      if x == 0:
        x = 1
      elif x == 32:
        x = 31
      elif y == 0:
        y = 1
      elif y == 32:
        y = 31
      self.start_position = (x, y)

    #### Populate rooms ####
    room_map = {}
    for room in xrange(0, next_room_id):
      if room not in hallways:
        room_map[room] = libtcod.random_get_int(self.random, 0, 4)

    for i in xrange(len(xlines) - 1):
      for j in xrange(len(ylines) - 1):
        if claims[(i,j)] not in room_map:
          continue
        room_type = room_map[claims[(i,j)]]

        if room_type == 0: # library
          arragement = libtcod.random_get_int(self.random, 0, 4)
          if arragement < 2:
            for x in xrange(2 * xlines[i] + 2, 2 * xlines[i+1] - 1):
              for y in xrange(2 * ylines[j], 2 * ylines[j+1] + 1, 2):
                if isinstance(floor[x][y], tiles.FloorTile):
                  floor[x][y] = tiles.WallTile()
          elif arragement < 4:
            for x in xrange(2 * xlines[i], 2 * xlines[i+1] + 1, 2):
              for y in xrange(2 * ylines[j] + 2, 2 * ylines[j+1] - 1):
                if isinstance(floor[x][y], tiles.FloorTile):
                  floor[x][y] = tiles.WallTile()

        elif room_type == 1: # storage
          if libtcod.random_get_int(self.random, 0, 2) < 2:
            for x in xrange(2 * xlines[i] + 1, 2 * xlines[i+1]):
              for y in xrange(2 * ylines[j] + 1, 2 * ylines[j+1]):
                if libtcod.random_get_int(self.random, 0, 3) == 0:
                  floor[x][y] = tiles.BoxTile()
                  valuables = libtcod.random_get_int(self.random, 0, 4) + \
                              libtcod.random_get_int(self.random, 0, 4) - 4
                  for i in xrange(max(0, valuables)):
                    floor[x][y].add_item(items.Valuable(len(self.floors),
                                                        self.random))
                  if libtcod.random_get_int(self.random, 0, 24) == 0:
                    floor[x][y].add_item(items.Torch())

        elif room_type == 2: # pillars
          for x in [2 * xlines[i] + 2, 2 * xlines[i+1] - 2]:
            for y in [2 * ylines[j] + 2, 2 * ylines[j+1] - 2]:
              floor[x][y] = tiles.WallTile()

    max_bodies = float(MAX_BODIES * MAX_BODIES_SCALING) / (MAX_BODIES_SCALING + len(self.floors)) + 1
    min_bodies = float(MAX_BODIES * MIN_BODIES_SCALING) / (MIN_BODIES_SCALING + len(self.floors))

    bodies = int(libtcod.random_get_float(self.random, min_bodies, max_bodies))

    for i in xrange(bodies):
      while True:
        x = libtcod.random_get_int(self.random, 1, 31)
        y = libtcod.random_get_int(self.random, 1, 31)
        if isinstance(floor[x][y], tiles.FloorTile):
          floor[x][y] = tiles.BodyTile()
          for i in xrange(libtcod.random_get_int(self.random, 2, 7)):
            item_type = libtcod.random_get_int(self.random, 0, 7)
            if item_type < 5:
              floor[x][y].add_item(items.Valuable(len(self.floors), self.random))
            elif item_type < 7:
              floor[x][y].add_item(items.BurntTorch())
            else:
              floor[x][y].add_item(items.Torch())
          break

    #### Update state ####
    self.floors.append(floor)
    self.top_stairs = next_stairs


  def subdivide_intervals(self, points):

    point_set = set(points)
    sorted_points = sorted(points)
    intervals = set(zip(sorted_points[:-1], sorted_points[1:]))

    while intervals:
      a, b = intervals.pop()
      if b - a <= 4:
        continue
      c = libtcod.random_get_int(self.random, a + 1, b - 1)
      point_set.add(c)
      intervals.add((a,c))
      intervals.add((c,b))

    return sorted(point_set)


  def get_floor(self, n):
    while n >= len(self.floors):
      self.gen_next_floor()

    return self.floors[n]


  def get_start_position(self):
    return self.start_position


