This game was made over the cource of 3 days for the 2013 7DRL Challenge,
so needless to say it's a little rough around the edges. The goal is to
explore a haunted tower and get out with as much loot as possible. If
your torches burn out then the ghosts will start stealing your stuff.
It requires python 2 and libtcod.


Controls are as follows:

movement - arrow keys, number keys, vi-keys, qwe/asd/zxc - take your pick
pick up item - use ',' or 'p' when over a body (gray @) or a box (brown square)
ignite a new torch - 'i' (note that the partially burnt torches may be flaky)
select - enter or space bar
cancel or quit - escape
