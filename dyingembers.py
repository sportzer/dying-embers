#!/usr/bin/python
import libtcodpy as libtcod
import mapgen
import tiles
import items
 
LIMIT_FPS = 20
MAX_LIGHT_RADIUS = 7

MAX_TORCH_TURNS = 256
MIN_TORCH_TURNS = 192
TORCH_BURNT_OUT_CHANCE = 0.4


KEYBINDINGS = {
  "up"         : [libtcod.KEY_UP, libtcod.KEY_8, libtcod.KEY_KP8, 'k', 'w'],
  "down"       : [libtcod.KEY_DOWN, libtcod.KEY_2, libtcod.KEY_KP2, 'j', 'x'],
  "left"       : [libtcod.KEY_LEFT, libtcod.KEY_4, libtcod.KEY_KP4, 'h', 'a'],
  "right"      : [libtcod.KEY_RIGHT, libtcod.KEY_6, libtcod.KEY_KP6, 'l', 'd'],
  "up_left"    : [libtcod.KEY_7, libtcod.KEY_KP7, 'y', 'q'],
  "up_right"   : [libtcod.KEY_9, libtcod.KEY_KP9, 'u', 'e'],
  "down_left"  : [libtcod.KEY_1, libtcod.KEY_KP1, 'b', 'z'],
  "down_right" : [libtcod.KEY_3, libtcod.KEY_KP3, 'n', 'c'],
  "rest"       : [libtcod.KEY_5, libtcod.KEY_KP5, '.', 's'],
  "pickup"     : [',', 'p'],
  "ignite"     : ['i'],
  "cancel"     : [libtcod.KEY_ESCAPE],
  "select"     : [libtcod.KEY_ENTER, ' '],
}



class Game():

  def __init__(self):

    self.random = libtcod.random_get_instance()
    self.tower_map = mapgen.Map(self.random)

    self.player_floor = 0
    self.player_x, self.player_y = self.tower_map.get_start_position()
    self.light_left = libtcod.random_get_int(self.random, MIN_TORCH_TURNS,
                                             MAX_TORCH_TURNS)
    self.player_torches = 2
    self.player_burnt_torches = 0
    self.player_inventory = []
    self.player_money = 0

    self.floors_seen = []
    self.fov_map = None
    self.light_radius = None
    self.compute_fov()

    self.status_text = ""
    self.ended = False
    self.play_again = None

    self.ghosts = []
    self.advance_time(0)
    self.dead = False


  def set_status(self, text):
    self.status_text = text


  def clear_status(self):
    self.status_text = ""


  def render_floor(self):

    floor = self.tower_map.get_floor(self.player_floor)

    for x in xrange(0, 33):
      for y in xrange(0, 33):
        if libtcod.map_is_in_fov(self.fov_map, x, y) and \
            (self.player_x - x)**2 + (self.player_y - y)**2 <= \
            self.light_radius**2 + self.light_radius:
          letter, color = floor[x][y].get_display()
          libtcod.console_set_char_background(0, x + 3, y + 3, libtcod.darkest_gray,
                                              flag=libtcod.BKGND_SET)
          libtcod.console_set_char_foreground(0, x + 3, y + 3, color)
          libtcod.console_set_char(0, x + 3, y + 3, letter)
        else:
          libtcod.console_set_char_foreground(0, x + 3, y + 3, libtcod.darker_gray)
          libtcod.console_set_char(0, x + 3, y + 3,
                                   self.floors_seen[self.player_floor][x][y])

    for x, y in self.ghosts[self.player_floor]:
      if libtcod.map_is_in_fov(self.fov_map, x, y) and (self.light_left == 0 or
          (self.player_x - x)**2 + (self.player_y - y)**2 >
           self.light_radius**2 + self.light_radius):
        libtcod.console_set_char_foreground(0, x + 3, y + 3, libtcod.white)
        libtcod.console_set_char(0, x + 3, y + 3, "G")

    libtcod.console_set_char_foreground(0, self.player_x + 3,
                                        self.player_y + 3, libtcod.green)
    libtcod.console_set_char(0, self.player_x + 3, self.player_y + 3, "@")

    self.render_status()


  def render_status(self):

    libtcod.console_print(0, 0, 0, "Floor: %d" % (self.player_floor + 1))
    libtcod.console_print(0, 0, 1, "Loot: $%d" % self.player_money)

    libtcod.console_print(0, 21, 0, "Torches:       %d" % self.player_torches)
    libtcod.console_print(0, 21, 1, "Burnt Torches: %d" % self.player_burnt_torches)

    libtcod.console_print_rect(0, 0, 37, 39, 2, self.status_text)


  def compute_fov(self):

    self.light_radius = max(1, min(self.light_left/4, MAX_LIGHT_RADIUS))

    floor = self.tower_map.get_floor(self.player_floor)
    fov_map = libtcod.map_new(33, 33)

    for x in xrange(0, 33):
      for y in xrange(0, 33):
        passable = floor[x][y].is_passable()
        libtcod.map_set_properties(fov_map, x, y, passable, passable)

    libtcod.map_compute_fov(fov_map, self.player_x, self.player_y)

    while self.player_floor >= len(self.floors_seen):
      self.floors_seen.append([[None for i in xrange(0, 33)]
                               for j in xrange(0, 33)])

    for x in xrange(0, 33):
      for y in xrange(0, 33):
        if (self.player_x - x)**2 + (self.player_y - y)**2 <= \
            self.light_radius**2 + self.light_radius:
          if libtcod.map_is_in_fov(fov_map, x, y):
            letter, color = floor[x][y].get_display()
            self.floors_seen[self.player_floor][x][y] = letter

    self.fov_map = fov_map


  def move_player(self, dx, dy):

    self.clear_status()

    floor = self.tower_map.get_floor(self.player_floor)
    next_tile = floor[self.player_x + dx][self.player_y + dy]

    if next_tile.is_passable():

      self.advance_time(1)

      self.player_x += dx
      self.player_y += dy

      direction = None
      if isinstance(next_tile, tiles.UpTile):
        self.set_status("You go up a floor.")
        self.player_floor += 1
        direction = (next_tile.rot + 1) % 4
      elif isinstance(next_tile, tiles.DownTile):
        self.set_status("You go down a floor.")
        self.player_floor -= 1
        direction = (next_tile.rot + 3) % 4

      if direction == 0:
        self.player_x += 3
      elif direction == 1:
        self.player_y -= 3
      elif direction == 2:
        self.player_x -= 3
      elif direction == 3:
        self.player_y += 3

      self.advance_time(0)
      self.compute_fov()

    elif isinstance(next_tile, tiles.DoorTile):
      if next_tile.is_locked:
        self.advance_time(8)
        if libtcod.random_get_int(self.random, 0, 3) == 0:
          next_tile.is_open = True
          next_tile.is_locked = False
          self.set_status("The door resists your attempts to open it, " +
                          "but you manage to bash it open.")
        else:
          self.set_status("The door resists your attempts to open it.")
        self.compute_fov()

      else:
        self.advance_time(4)
        next_tile.is_open = True
        self.set_status("You open the door.")
        self.compute_fov()

    elif isinstance(next_tile, tiles.ExitDoorTile):
      if confirm_action("exit the tower (ending your game)", self):
        self.end()

    else:
      self.set_status("You can't move there.")


  def rest(self):
    self.advance_time(1)
    self.set_status("You rest for a turn.")


  def advance_time(self, turns):

    floor = self.tower_map.get_floor(self.player_floor)
    current_tile = floor[self.player_x][self.player_y]

    while self.player_floor >= len(self.ghosts):
      level_ghosts = []
      for i in xrange(libtcod.random_get_int(self.random, 0, 4)):
        x = libtcod.random_get_int(self.random, 0, 32)
        y = libtcod.random_get_int(self.random, 0, 32)
        level_ghosts.append((x,y))
      self.ghosts.append(level_ghosts)

    for x in xrange(turns):
      self.light_left = max(0, self.light_left - 1)

      self.compute_fov()

      ghost_list = self.ghosts[self.player_floor]
      for i in xrange(len(ghost_list)):
        x, y = ghost_list[i]
        if self.light_left == 0 and libtcod.random_get_int(self.random, 0, 3) < 3:
          if self.player_x > x:
            x += 1
          elif self.player_x < x:
            x -= 1
          if self.player_y > y:
            y += 1
          elif self.player_y < y:
            y -= 1
        else:
          x += libtcod.random_get_int(self.random, -1, 1)
          y += libtcod.random_get_int(self.random, -1, 1)

        x = max(min(x, 32), 0)
        y = max(min(y, 32), 0)

        if self.light_left == 0 and (x, y) == (self.player_x, self.player_y):
          if self.player_inventory:
            stolen_item = max(self.player_inventory, key = lambda i: i.value)
            self.player_inventory.remove(stolen_item)
            self.player_money -= stolen_item.value
            item_name = "your " + stolen_item.get_name()
          elif self.player_torches:
            self.player_torches -= 1
            item_name = "a torch"
          elif self.player_burnt_torches:
            self.player_burnt_torches -= 1
            item_name = "a partially burnt torch"
          else:
            self.dead = True
            self.end()
            return

          self.set_status("A ghost stole " + item_name + "! Press enter to continue.")
          while not libtcod.console_is_window_closed():
            libtcod.console_clear(0)
            self.render_floor()
            libtcod.console_flush()

            action = get_next_action()
            if action == 'select':
              break

          self.clear_status()

          x = libtcod.random_get_int(self.random, 0, 32)
          y = libtcod.random_get_int(self.random, 0, 32)

        ghost_list[i] = (x, y)

      libtcod.console_clear(0)
      self.render_floor()
      libtcod.console_flush()


  def end(self):
    self.ended = True

    while not libtcod.console_is_window_closed():
      libtcod.console_clear(0)
      if self.dead:
        libtcod.console_print_rect(0, 3, 15, 33, 4, "The ghosts stole all " +
                      "your stuff and then killed you. You are dead. Press " +
                      "enter to play again or escape to quit.")
      else:
        libtcod.console_print_rect(0, 3, 15, 33, 4, "You made it out with $" +
                      str(self.player_money) +
                      " worth of stuff! Press enter to play again or escape to quit.")
      libtcod.console_flush()

      action = get_next_action()

      if action == 'select':
        self.play_again = True
        return
      elif action == 'cancel':
        self.play_again = False
        return


  def pickup(self):

    floor = self.tower_map.get_floor(self.player_floor)
    tile = floor[self.player_x][self.player_y]

    if tile.items:
      selected_item = select_item("pick up", tile.items, self)
      if not selected_item:
        return

      self.advance_time(4)

      tile.remove_item(selected_item)
      self.set_status("You pick up the " + selected_item.get_name() + ".")

      if isinstance(selected_item, items.Torch):
        self.player_torches += 1
      elif isinstance(selected_item, items.BurntTorch):
        self.player_burnt_torches += 1
      else:
        self.player_money += selected_item.value
        self.player_inventory.append(selected_item)

    else:
      self.set_status("There are no items to pick up here.")


  def ignite(self):

    if self.player_torches > 0:
      self.advance_time(8)

      self.player_torches -= 1
      self.set_status("You light a new torch.")
      self.light_left = libtcod.random_get_int(self.random, MIN_TORCH_TURNS,
                                               MAX_TORCH_TURNS)

    elif self.player_burnt_torches > 0:
      self.advance_time(8)

      if libtcod.random_get_float(self.random, 0, 1) < TORCH_BURNT_OUT_CHANCE:
        self.player_burnt_torches -= 1
        self.set_status("The partially burnt torch fails to light.")
      else:
        self.player_burnt_torches -= 1
        self.set_status("You light a partially burnt torch.")
        self.light_left = libtcod.random_get_int(self.random, 0, MIN_TORCH_TURNS)

    else:
      self.set_status("You don't have any torches.")
      return

    self.compute_fov()



def get_next_action():

  key = libtcod.console_wait_for_keypress(True)

  if not key.pressed:
    return None

  for action in KEYBINDINGS:
    for binding in KEYBINDINGS[action]:
      if isinstance(binding, int) and binding == key.vk:
        return action
      elif isinstance(binding, str) and ord(binding) == key.c:
        return action

  return None



def confirm_action(action, game):

  try:
    game.set_status("Press enter to " + action + " or escape to cancel action.")

    while not libtcod.console_is_window_closed():
      libtcod.console_clear(0)
      game.render_floor()
      libtcod.console_flush()

      action = get_next_action()

      if action == 'select':
        return True
      elif action == 'cancel':
        return False

  finally:
    game.clear_status()



def get_direction(action, game):

  try:
    game.set_status("In what direction do you want to " + action + "?")

    while not libtcod.console_is_window_closed():
      libtcod.console_clear(0)
      game.render_floor()
      libtcod.console_flush()

      action = get_next_action()

      if action == "up":
        return (0, -1)
      elif action == "down":
        return (0, 1)
      elif action == "left":
        return (-1, 0)
      elif action == "right":
        return (1, 0)
      elif action == "up_left":
        return (-1, -1)
      elif action == "up_right":
        return (1, -1)
      elif action == "down_left":
        return (-1, 1)
      elif action == "down_right":
        return (1, 1)
      elif action == "cancel":
        return

  finally:
    game.clear_status()



def select_item(action, item_list, game):
  try:
    game.set_status("Which item do you want to " + action + "?")
    selected = 0

    while not libtcod.console_is_window_closed():
      libtcod.console_clear(0)
      game.render_status()

      for i in xrange(len(item_list)):
        if i == selected:
          libtcod.console_set_default_foreground(0, libtcod.green)

        libtcod.console_print(0, 1, i + 3, item_list[i].get_name())

        if isinstance(item_list[i], items.Valuable):
          libtcod.console_set_alignment(0, libtcod.RIGHT)
          libtcod.console_print(0, 37, i + 3, "$%d" % item_list[i].value)
          libtcod.console_set_alignment(0, libtcod.LEFT)

        libtcod.console_set_default_foreground(0, libtcod.lightest_grey)

      libtcod.console_flush()

      action = get_next_action()

      if action == "up":
        selected -= 1
      elif action == "down":
        selected += 1
      elif action == "select":
        return item_list[selected]
      elif action == "cancel":
        return

      selected %= len(item_list)

  finally:
    game.clear_status()



def movement_loop(game):

  while not libtcod.console_is_window_closed() and not game.ended:
    libtcod.console_clear(0)
    game.render_floor()
    libtcod.console_flush()

    action = get_next_action()

    if action == "up":
      game.move_player(0, -1)
    elif action == "down":
      game.move_player(0, 1)
    elif action == "left":
      game.move_player(-1, 0)
    elif action == "right":
      game.move_player(1, 0)
    elif action == "up_left":
      game.move_player(-1, -1)
    elif action == "up_right":
      game.move_player(1, -1)
    elif action == "down_left":
      game.move_player(-1, 1)
    elif action == "down_right":
      game.move_player(1, 1)
    elif action == "rest":
      game.rest()
    elif action == "cancel":
      if confirm_action("quit the game ", game):
        break
    elif action == "pickup":
      game.pickup()
    elif action == "ignite":
      game.ignite()



def main():

  libtcod.console_init_root(39, 39, "Burning Embers")
  libtcod.sys_set_fps(LIMIT_FPS)

  libtcod.console_set_default_background(0, libtcod.black)
  libtcod.console_set_default_foreground(0, libtcod.lightest_grey)

  while True:
    game = Game()
    movement_loop(game)

    if game.ended and game.play_again:
      pass
    else:
      break


if __name__ == "__main__":
  main()


